import express, { Request, Response }  from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import dotenv from 'dotenv';
import cors from 'cors';
dotenv.config();

import CalculateController from './src/Controllers/Calculate.controller';

const app = express();

app.use(cors({
    origin: '*',
    allowedHeaders: ['Access-Control-Allow-Headers']
}));
app.use('/js', express.static('build/assets/ts'));
app.use(bodyParser.json({ limit: '1mb' }));

app.get('/', function(req, res) {
    return res.sendFile(path.resolve(__dirname + '/../assets/index.html'))
});

app.get('/hello', function(request: Request, response: Response): Response {
    return response.send(CalculateController.routeHelloWorld(request.query));
});

app.get('/api/vegetables', function(request: Request, response: Response): Response {
    return response.send(CalculateController.routeVegetable());
});

app.post('/api/sum', function (request: Request, response: Response): Response {
    return response.send(CalculateController.routeSum(request.body));
});

app.listen(process.env.PORT || 3000, function() {
    console.error(`App started ${process.env.PORT}`);
});
