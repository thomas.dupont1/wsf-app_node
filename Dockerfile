FROM node:12

WORKDIR /app
COPY . /app

RUN npm install -g typescript
RUN npm install -g ts-node
RUN npm install express
