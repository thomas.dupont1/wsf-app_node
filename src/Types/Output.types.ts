
export type HelloOutput = {
    text: string
}

export type SumOutput = {
    sum: number
}
