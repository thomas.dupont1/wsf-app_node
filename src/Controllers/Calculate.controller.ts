import { sum } from '../Managers/Operations.manager';
import { HelloOutput, SumOutput } from '../Types/Output.types';
import { SumInput } from '../Types/Input.types';

export default class CalculateController {
    /**
     * @route '/hello'
     * @get
     */
    public static routeHelloWorld(query: any): HelloOutput {
        console.log(query);
        return {
            text: 'Hello World'
        };
    }

    /**
     * @route '/api/vegetables'
     * @get
     */
    public static routeVegetable(): string[] {
        return [
            'Carottes',
            'Patattes',
            'Concombres',
            'Salades',
            'Navets'
        ];
    }

    /**
     * @route '/api/sum'
     * @post
     */
    public static routeSum(body: SumInput): SumOutput {
        return {
            sum: sum(body.a, body.b)
        }
    }
}
