# Disruptive Calculate

Link: [https://gitlab.com/thomas.dupont1/wsf-app_node/-/tree/master]

## Installation

### Pre-requistes

Docker
Or
Node 12 + typescript (npm i -g typescript)

### Docker

```sh
docker-compose up
```

### Local

```sh
npm install
```

```sh
cp .env.test .env
```

```sh
npm run dev
```

## Usage
